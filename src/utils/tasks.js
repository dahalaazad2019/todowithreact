export const TASKS = [
    {id: 21,title: 'Task 1',description: 'description1',status: 'Todo'},
    {id: 32,title: 'Task 2',description: 'description2',status: 'In-Progress'},
    {id: 53,title: 'Task 3',description: 'description3',status: 'Todo'},
    {id: 47,title: 'Task 4',description: 'description4',status: 'Review'},
    {id: 58,title: 'Task 5',description: 'description5',status: 'Todo'},
    {id: 63,title: 'Task 6',description: 'description6',status: 'Completed'},
    {id: 67,title: 'Task 7',description: 'description6',status: 'Review'},      
    {id: 23,title: 'Task 21',description: 'description6',status: 'In-Progress'},      
];

export const statusList = ['Todo', 'In-Progress', 'Review', 'Completed'];