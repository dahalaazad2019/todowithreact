//import { useState } from "react";
//import { Link } from "react-router-dom";
//import { Link } from "react-router-dom";
import "../assets/css/layout.css";
//import { TASKS } from "../utils/tasks.js";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";


//console.log(listOfTasks);
export const TodoCard = ({ currentTask ,deleteTask,id }) => {
  //const [selectedId, setSelectedId] = useState("-1");
  // <div className="task-card">
  //console.log('Double passed Props',props.tasks)
  //console.log(id)
  return (
    <>
    
      <div className="task-card-container items-center" key={currentTask.id}>
        <div className="task-card card flex">
          <div className="task-content">
            <div className="id">ID: {currentTask.id}</div>
            <div className="title">{currentTask.title}</div>
            <div className="description">{currentTask.description}</div>
          </div>
          <div className="task-actions flex">
          <Link to={'/edit'}
              state={{currentTask}}
          >
          <button onClick={() => {console.log(id)} }>Edit</button>
          </Link>
              
            
            <button onClick={() =>deleteTask(currentTask.id)}>Delete</button>
          </div>
        </div>
      </div>
    </>
  );
};
