import { useState } from "react";


export const AddTask = ({tasks,setTasks,appendTask}) => {
  //const [newTask, setNewTask] = useState({});
  const [task, setTask] = useState({
    id:(new Date()).toISOString(),title:'',desciption:'',status:'Todo'
  });
  const handleInputChange = (event) => {
    setTask({
      ...task,
      [event.target.name]: event.target.value
    });
    console.log(task);
  };
  //setTasks([...tasks,task]);
  const createTask=(e)=>{
    e.preventDefault();
    appendTask(task);

}
  
  return (
    <>
      <form onSubmit={createTask}>
        {/* <label>
          Id:
          <input type="text" name="id" onChange={handleInputChange} placeholder={'Enter Id'}  value={task.id} />
        </label> */}
        <br />
        <label>
          Title:
          <input type="text" name="title" onChange={handleInputChange} placeholder={'Enter title'}  value={task.title} />
        </label>

        <br />
        <label>
          Description:
          <input type="text" name="description" onChange={handleInputChange} placeholder={'Enter Description'}  value={task.description} />
        </label>
        <br />
        <label>
          Status--
          <select name="status"  onChange={handleInputChange} value={task.status}>
            <option value disabled>
              -- Select a Status --
            </option>
            <option value="Todo">Todo</option>
            <option value="In-Progress">In-Progress</option>
            <option value="Review">Review</option>
            <option value="Completed">Completed</option>
          </select>
        </label>
        <br />
        <button type="submit">Add Task</button>
      </form>
    </>
  );
};
