import { useState } from "react";
import "../assets/css/layout.css";
import { TodoCard } from "./TodoCard";
//import { statusList } from "../utils/tasks";
import { TASKS } from "../utils/tasks";
import { AddTask } from "./AddTask";
import { EditTask } from "./EditTask";


export const TodoStatusColumns = (props) => {
  const [tasks, setTasks] = useState([...TASKS]);
  

  //const [taskStatus, setTaskStatus] = useState("");
  //let tasks = props.tasks;
  //console.log(listOfStatus);
  //console.log("here-->", props.tasks);
  const appendTask = (task) => {
    console.log(task);
    setTasks([...tasks,task])
  }
  const deleteTask = (id) => {
    const tempTasks = tasks.filter((item) => item.id !== id);
    setTasks([...tempTasks]);
  };

  return (
    <>
      <div className="add-edit flex flex-1">
      <div className="add">
      <h1>Add Form</h1>
      <AddTask tasks={tasks} setTasks={setTasks} appendTask={appendTask}/>
      </div>
      </div>

      <div className="tasks-area flex flex-1">
        {props.statusList.map((status, index) => (
          //console.log(status)
          <div className="task-column " key={index}>
            <div className="column-title">{status}</div>
            <div className="todo-content">
              {tasks
                .filter((task) => task.status === status)
                .map((currentTask) => (
                  //INSERT HERE TASK CARDS ACCORDING TO STATUS COLUMN
                  <>
                    <TodoCard
                    key={currentTask.id}
                    id={currentTask.id}
                    currentTask={currentTask}
                    deleteTask={deleteTask}
                  />
                  
                  </>
                  
                ))}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};
