import { useLocation } from "react-router-dom";

export const EditTask = (props) => {
  const location = useLocation()
  const {currentTask} = location.state
  console.log(currentTask);

  const updateTask=(e)=>{
    e.preventDefault();
    // appendTask(task);

}

  //const onEdit = (event) => [event.target.name]: event.target.value
  return (
    <> 
      <div className="edit">
        <h1>Edit Form</h1>
        <form onSubmit={updateTask} >
        <label>
          Id:
          <input type="text" name="id" defaultValue={currentTask.id} disabled/>
        </label>{" "}
        <br />
        <label>
          Title:
          <input type="text" name="title" defaultValue={currentTask.title} />
        </label>
        <br />
        <label>
          Description:
          <input type="text" name="description" defaultValue={currentTask.description} />
        </label>
        <br />
        <label>
          Status--
          <select name="status" defaultValue={currentTask.status} >
            <option value disabled >
              -- Select a Status
            </option>
            <option value="Todo">Todo</option>
            <option value="In-Progress">In-Progress</option>
            <option value="Review">Review</option>
            <option value="Completed">Completed</option>
          </select>
        </label>
        <br />
        <button type="submit">Update Task</button>
      </form>
      </div>
    </>
  );
};
