import "./App.css";
import { Landing } from "./pages/Landing";
//import { TASKS } from "./utils/tasks";
import "./assets/css/vars.css";
import "./assets/css/layout.css";
import "./assets/css/theme.scss";
import "./assets/css/style.css";
//import { AddTask } from "./components/AddTask";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { TodoCard } from "./components/TodoCard";
import { EditTask } from "./components/EditTask";
//import { EditTask } from "./components/EditTask";

function App() {
  //console.log(TASKS);
  return (
    <Router>
      <Routes>
        <Route path='/edit' element={<EditTask/>} />
      </Routes>
      <Landing />
      
      
    </Router>
    
  );
}

export default App;
